import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Posts from './src/Posts';
import Usuarios from './src/Usuarios';

 const screenOptions = ({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName ;

            if (route.name === 'Usuarios') {
              iconName = focused ? 'person-outline': 'person';
            } else if (route.name === 'Posts') {
              iconName = focused ? 'list' : 'list-alt';
            }

            return (<MaterialIcons name={iconName} size={size} color={color} />)
          }
 })

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer >
      <Tab.Navigator  screenOptions={screenOptions}>
        <Tab.Screen name="Usuarios" component={Usuarios} />
        <Tab.Screen name="Posts" component={Posts} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
