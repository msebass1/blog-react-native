import * as React from 'react';
import moment from 'moment'; 
import { Picker } from '@react-native-picker/picker';
import { Text, View, StyleSheet, StatusBar, Button, FlatList, Modal, TextInput } from 'react-native';
import { usersCollection, postsCollection, database } from './models/index';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Item = ({ title, content, navigation, data }) => (
  <View style={styles.item} >
    <Text style={styles.title}>{title}</Text>
    <Text>{content.slice(0, 50)}</Text>
    <Button title='detalle' onPress={()=>navigation.navigate('detalle',{data})}/>
  </View>
);

const Modall = ({ visible, users, onClose }) =>{
  const [title,setTitle] = React.useState('');
  const [content,setContent] = React.useState('');
  const [user_id,setUser_id] = React.useState('');

  return (
    <Modal
        animationType="slide"
        transparent={true}
        visible={visible}
        onRequestClose={() => onClose()}
      >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={[styles.modalText,styles.pad]}>Nuevo usuario</Text>
	  <TextInput
	    style={[styles.textInput,styles.pad]}
	    placeholder="Escribe el Titulo!"
	    onChangeText={text => setTitle(text)}
	    defaultValue={title}/>
	  <TextInput
	    multiline={true}
	    numberOfLines={4}
	    style={[styles.textInput,styles.pad]}
	    placeholder="Escribe el contenido del post!"
	    onChangeText={text => setContent(text)}
	    defaultValue={content}/>
	  <Picker
	    selectedValue={user_id}
	    style={{height: 50, width: 100}}
	    onValueChange={(itemValue, itemIndex) =>
	      setUser_id(itemValue)
	  }>
	    {users.map((item)=>
	      <Picker.Item label={item._raw.name} value={item._raw.id} key={item._raw.id}/>
	    )}
	  </Picker>
	  <Button  title='Crear nuevo Post' onPress={()=>{crearPost(title, content, user_id);onClose()}}/>
	  <Button  color="red" title='cancelar' onPress={()=>onClose()}/>
        </View>
      </View>
    </Modal>
  )
}

async function crearPost (title,content,user_id){
  await database.action(async () => {
    const newPost = await postsCollection.create(user => {
      user._raw['user_id'] = user_id
      user._raw['title'] = title
      user._raw['content'] = content
      user._raw['created_at'] = moment().unix()
    })
  })
}


function Psts({ navigation }){
  const [mod,setMod] = React.useState(false)
  const [usr,setUsr] = React.useState([])
  React.useEffect(()=>{
    usersCollection.query().fetch()
      .then((allUsers)=>setUsr(allUsers))
  },[mod])

  const [posts,setPosts] = React.useState([])
  React.useEffect(()=>{
    postsCollection.query().fetch()
      .then((allPosts)=>setPosts(allPosts))
  },[mod])


  const renderItem = ({ item }) => (
    <Item title={item._raw.title} data={item} navigation={navigation} content={item._raw.content} />
  );
  
  return (
    <View style={styles.container}>
    <Button title='+ Crear nuevo post' onPress={()=>setMod(true)}/>
      <FlatList
	data={posts}
        renderItem={renderItem}
        keyExtractor={item => item._raw.id}
      />
      <Modall visible={mod} users={usr} onClose={()=>setMod(false)}/>
    </View>
  );

}

function Detail({ route,navigation }){
  const { data } = route.params;
  return(
    <View>
      <Text>{data._raw.title}</Text>
      <Text>{data._raw.user_id}</Text>
      <Text>{moment.unix(data._raw.created_at).format("MM/DD/YYYY") }</Text>
      <Text>{data._raw.content}</Text>
    </View>
  )
}

const MainStack = createStackNavigator();

export default function Posts() {
  return(
    <MainStack.Navigator>
      <MainStack.Screen name="Posts" options={{headerShown: false}} component={Psts} />
      <MainStack.Screen name="detalle" component={Detail} />
    </MainStack.Navigator>
  )
}

const styles = StyleSheet.create({
  pad:{
    marginVertical:15
  },
  container: {
    flex: 1,
    //marginTop: StatusBar.currentHeight || 0,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  textInput:{
    borderBottomWidth:3,
    borderBottomColor: '#ccc',
    backgroundColor: '#eee',
  },
  item: {
    borderBottomWidth:3,
    borderBottomColor: '#ccc',
    backgroundColor: '#eee',
    padding: 15,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 20,
  },
  content: {
    fontSize: 12,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "stretch",
    shadowColor: "#000",
    width:250,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
