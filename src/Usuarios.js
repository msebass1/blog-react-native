import * as React from 'react';
import { Text, View, FlatList, StyleSheet, StatusBar, Button, Modal, TextInput } from 'react-native';
import { usersCollection, database } from './models/index';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

const Item = ({ nombre, apellido }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{`${nombre} ${apellido}`}</Text>
  </View>
);

const Modall = ({ visible, onClose }) =>{
  const [name,setName] = React.useState('')
  const [last_name,setLast_name] = React.useState('')
  return (
    <Modal
        animationType="slide"
        transparent={true}
        visible={visible}
        onRequestClose={() => onClose()}
      >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={[styles.modalText,styles.pad]}>Nuevo usuario</Text>
	  <TextInput
	    style={[styles.textInput,styles.pad]}
	    placeholder="Escribe el nombre 🍕!"
	    onChangeText={text => setName(text)}
	    defaultValue={name}/>
	  <TextInput
	    style={[styles.textInput,styles.pad]}
	    placeholder="Escribe el apellido 🍕!"
	    onChangeText={text => setLast_name(text)}
	    defaultValue={last_name}/>
	  <Button  title='Crear nuevo usuario' onPress={()=>{crearUsuario(name, last_name);onClose()}}/>
	  <Button  color="red" title='cancelar' onPress={()=>onClose()}/>
        </View>
      </View>
    </Modal>
  )
}

async function crearUsuario (name,last_name){
  await database.action(async () => {
    const newUser = await usersCollection.create(user => {
      user._raw['name'] = name
      user._raw['last_name'] = last_name
    })
  })

}

function Usrs (){
  const [usr,setUsr] = React.useState([])
  const [mod,setMod] = React.useState(false)

  const renderItem = ({ item }) => (
    <Item nombre={item._raw.name} apellido={item._raw.last_name} />
  );

  React.useEffect(()=>{
    usersCollection.query().fetch()
      .then((allPosts)=>setUsr(allPosts))
  },[mod])


  return (
    <View style={styles.container}>
      <Button title='+ Crear nuevo usuario' onPress={()=>setMod(true)}/>
      <FlatList
	data={usr}
        renderItem={renderItem}
        keyExtractor={item => item._raw.id}
      />
      <Modall visible={mod} onClose={()=>setMod(false)}/>
    </View>
  );
}

function Notifications (){
  return (
    <View style={styles.container}>
      <Text>Notificaciones</Text>
    </View>
  )
}

const Drawer = createDrawerNavigator();

export default function Usuarios() {
  return (
      <Drawer.Navigator initialRouteName="users" >
        <Drawer.Screen name="Usuarios" component={Usrs} />
        <Drawer.Screen name="Notificaciones" component={Notifications} />
      </Drawer.Navigator>
  )
}

const styles = StyleSheet.create({
  pad:{
    marginVertical:15
  },
  container: {
    flex: 1,
    //marginTop: StatusBar.currentHeight || 0,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  textInput:{
    borderBottomWidth:3,
    borderBottomColor: '#ccc',
    backgroundColor: '#eee',
  },
  item: {
    borderBottomWidth:3,
    borderBottomColor: '#ccc',
    backgroundColor: '#eee',
    padding: 15,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "stretch",
    shadowColor: "#000",
    width:250,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
