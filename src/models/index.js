import { Database } from '@nozbe/watermelondb'
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite'

import schema from './schema'
import Post from './Post';
import Users from './Users';
// import Post from './model/Post' // ⬅️ You'll import your Models here

// First, create the adapter to the underlying database:
const adapter = new SQLiteAdapter({
  schema,
  dbName: 'proyectPrueba', 
})

// Then, make a Watermelon database from it!
export const database = new Database({
  adapter,
  modelClasses: [
    Post,
    Users
    // Post, // ⬅️ You'll add Models to Watermelon here
  ],
  actionsEnabled: true,
})

export const usersCollection = database.get('users');
export const postsCollection = database.get('posts');


