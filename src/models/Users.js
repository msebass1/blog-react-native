import {Model} from '@nozbe/watermelondb';
import {field, date, readonly} from '@nozbe/watermelondb/decorators';

export default class Users extends Model {
  static table = 'users';
  static associations = {
    posts: {type: 'has_many', key: 'user_id'},
  };
}
